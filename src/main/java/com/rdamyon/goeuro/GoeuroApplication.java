package com.rdamyon.goeuro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoeuroApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoeuroApplication.class, args);
	}
}
