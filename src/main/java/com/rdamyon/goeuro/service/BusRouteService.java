package com.rdamyon.goeuro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by rene on 28.05.17.
 */
@Component
public class BusRouteService {

    private int[][] busRoutes;

    /*
    Initialise should fail on lack of file detected
     */
    @Autowired
    public BusRouteService(@Value("${busroutes.path}") String path) throws IOException{
        Path filePath = Paths.get(path);
        populateRouteList(filePath);
    }

    /*
    Given the data size can potentially reach 100 million unique route and station ID combinations
    (100,000 routes and 1000 ID's per route) the base memory needed to store these as 32bit integers
    is approximately 380 MB. To maintain a sane memory limit this limits the use of maps and sets to
    provide constant time lookups however the data set is small enough that iterating through it to find
    a match is still quick enough to be responsive.

    Memory usage of the JVM on local machine is ~1.8gb with 100,000 routes, 1000 station ID's per route and
    1,000,000 unique station IDs. Server start up is 6-8 seconds.
     */
    private void populateRouteList(Path filePath) throws IOException{
        Iterator<String> fileIterator = Files.lines(filePath).iterator();
        Integer routeTotal = Integer.parseInt(fileIterator.next());
        busRoutes = new int[routeTotal][];
        int routeCount = 0;
        while(fileIterator.hasNext()) {
            String[] route = fileIterator.next().split(" ");
            busRoutes[routeCount] = new int[route.length - 1];
            for (int i = 1; i < route.length; i++) {
                busRoutes[routeCount][i-1] = Integer.parseInt(route[i]);
            }
            routeCount++;
        }
    }

    /*
    Assumption here is that routes are uni-directional, the arrival station must appear after the
    departure station in the bus route.
     */
    public Boolean checkDirectRoute(Integer depId, Integer arrId){
        if(busRoutes != null) {
            for (int[] route : busRoutes) {
                boolean depIdFound = false;
                for (int i = 0; i < route.length; i++) {
                    if (!depIdFound && route[i] == depId) {
                        depIdFound = true;
                    } else if (depIdFound && route[i] == arrId) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
