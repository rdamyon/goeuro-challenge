package com.rdamyon.goeuro.web;

import com.rdamyon.goeuro.service.BusRouteService;
import com.rdamyon.goeuro.web.model.BusRouteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by rene on 28.05.17.
 */
@RestController
public class BusRouteController {

    private BusRouteService busRouteService;

    @RequestMapping(value = "/api/direct", method = GET)
    public BusRouteResponse getBusRoute(@RequestParam(value="dep_sid") Integer depSid,
                                        @RequestParam(value="arr_sid") Integer arrSid){
        BusRouteResponse response = new BusRouteResponse(depSid,arrSid);
        response.setDirectBusRoute(busRouteService.checkDirectRoute(depSid, arrSid));
        return response;
    }

    @Autowired
    public void setBusRouteService(BusRouteService busRouteService) {
        this.busRouteService = busRouteService;
    }
}
