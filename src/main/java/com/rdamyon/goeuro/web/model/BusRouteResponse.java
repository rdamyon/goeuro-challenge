package com.rdamyon.goeuro.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rene on 28.05.17.
 */
public class BusRouteResponse {


    private Integer depSid;
    private Integer arrSid;
    private Boolean directBusRoute = false;

    public BusRouteResponse(Integer depSid, Integer arrSid){
        this.arrSid = arrSid;
        this.depSid = depSid;
    }

    @JsonProperty("dep_sid")
    public Integer getDepSid() {
        return depSid;
    }

    public void setDepSid(Integer depSid) {
        this.depSid = depSid;
    }

    @JsonProperty("arr_sid")
    public Integer getArrSid() {
        return arrSid;
    }

    public void setArrSid(Integer arrSid) {
        this.arrSid = arrSid;
    }

    @JsonProperty("direct_bus_route")
    public Boolean getDirectBusRoute() {
        return directBusRoute;
    }

    public void setDirectBusRoute(Boolean directBusRoute) {
        this.directBusRoute = directBusRoute;
    }


}
