package com.rdamyon.goeuro.service;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


/**
 * Created by rene on 30.05.17.
 */
public class BusRouteServiceTest {

    BusRouteService busRouteService;

    private static final String DATA_FILE_PATH = "src/test/resources/BusRouteDetails";

    @Test
    public void testPositiveResult() throws Exception{
        int depId = 148, arrId = 118;
        busRouteService = new BusRouteService(DATA_FILE_PATH);
        assertTrue(busRouteService.checkDirectRoute(depId, arrId));
    }

    @Test
    public void testNegativeResult()throws Exception{
        int depId = 148, arrId = 1180;
        busRouteService = new BusRouteService(DATA_FILE_PATH);
        assertFalse(busRouteService.checkDirectRoute(depId, arrId));
    }

    @Test
    public void testArrIdBeforeDepId()throws Exception{
        int depId = 118, arrId = 148;
        busRouteService = new BusRouteService(DATA_FILE_PATH);
        assertFalse(busRouteService.checkDirectRoute(depId, arrId));
    }

    @Test
    public void testArrIdEqualsDepId()throws Exception{
        int depId = 118, arrId = 118;
        busRouteService = new BusRouteService(DATA_FILE_PATH);
        assertFalse(busRouteService.checkDirectRoute(depId, arrId));
    }

    @Test
    public void testArrIdAndDepIdDifferentRoutes()throws Exception{
        int depId = 153, arrId = 174;
        busRouteService = new BusRouteService(DATA_FILE_PATH);
        assertFalse(busRouteService.checkDirectRoute(depId, arrId));
    }
}
