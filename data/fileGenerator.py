#!/usr/bin/python

with open("sampleData", "w") as file:
    busRouteId = 1
    stationId = 1
    file.write("100000\n")
    while busRouteId <= 100000 :
        busRoute = str(busRouteId)
        for x in range(1000):
            if stationId > 1000000 :
                stationId = 1
            busRoute += " " + str(stationId)
            stationId += 1
        busRoute += "\n"
        file.write(busRoute)
        busRouteId += 1
    file.close()