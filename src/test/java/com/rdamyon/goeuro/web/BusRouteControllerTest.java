package com.rdamyon.goeuro.web;

import com.rdamyon.goeuro.service.BusRouteService;
import com.rdamyon.goeuro.web.model.BusRouteResponse;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by rene on 30.05.17.
 */
public class BusRouteControllerTest {

    BusRouteController busRouteController;
    BusRouteService busRouteService;

    @Before
    public void setup(){
        busRouteController = new BusRouteController();
        busRouteService = mock(BusRouteService.class);
        busRouteController.setBusRouteService(busRouteService);
    }

    @Test
    public void testPositiveRequest(){
        Integer arrId = 1, depId = 2;
        when(busRouteService.checkDirectRoute(depId, arrId)).thenReturn(true);
        BusRouteResponse response = busRouteController.getBusRoute(depId, arrId);

        assertEquals(depId, response.getDepSid());
        assertEquals(arrId, response.getArrSid());
        assertTrue(response.getDirectBusRoute());
    }

    @Test
    public void testNegativeRequest(){
        Integer arrId = 1, depId = 2;
        when(busRouteService.checkDirectRoute(depId, arrId)).thenReturn(false);
        BusRouteResponse response = busRouteController.getBusRoute(depId, arrId);

        assertEquals(depId, response.getDepSid());
        assertEquals(arrId, response.getArrSid());
        assertFalse(response.getDirectBusRoute());
    }
}
